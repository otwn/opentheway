@def title = "About Me"

~~~
<div class="row">
<div class="col-12 text-center">
<img src="/assets/images/about/GulfGuardianAward2017SK.jpg" alt="my_photo" width="80%"/><br>
@2017 EPA Gulf Award Ceremony
</div>
</div>

<br>
<br>
~~~

## What I love to do

> When you know coding, it makes your analysis easier, faster, and better. It also helps to visualize your results through the website.
> GIS provides me opportunities to have a fun time for analysis, coding, visualization, and story-telling.
> I love GIS because it has all I want to do. That's why I've learned how to do `Geospatial Analysis`, how to utilize `GIS Server`, how to develop a `Web Map Application` to share and visualize results, and how to code for `packaging`, `modeling`, `automation` for pipeline development.
>
> I am enthusiastic about **Geospatial Data Science/Engineering and Visualization**, especially with maps. Focusing on Machine Learning using `Python,` `SQL` and `Julia`.

## Summary

Dr. Shinichi ("Shin") Kobara is a `Geospatial Data Scientist` with a Ph.D. in Geography and a certified Geographic Information System Professional (`GISP`). He has data engineering skills with experience in spatial analysis, spatial statistics, geo-visualization, geospatial predicting model development, and GIS server management.

- Over a decade of experience specializing in geospatial data analysis, model development
- Proficient in `SQL` (CTE, Window function,  user-defined functions, etc) and `Python` for algorithm development, feature engineering, ETL/EDA, CI/CD, API development, orchestration, testing, and implementation.
- Experience using data science tools and techniques for geospatial data product development and data visualization with `GCP BigQuery/GCS`, `AWS SageMaker/Lambda`, and `ArcGIS Enterprise/Portal`
- Applied research experience in Geospatial Analysis and hands-on experience in machine learning

### Resume

[PDF](/assets/pdfs/Shin-Kobara_Resume_GISP_2023_July.pdf)

## Experience

* Coordinate developing and administrating Geographic Information Systems (GIS) projects, including developing technical priorities, client reporting, interface, and coordination and reviewing schedules and budgets.
* Create, analyze, report, convert, or transfer data using specialized applications program software
* Design and develop python packages used by multiple teams
* Design, program, and model GIS/web map applications or procedures
* Refactor and/or customize SQL queries for several projects
* Lead, train, or supervise technicians or related staff in the conduct of Geographic Information Systems (GIS) analytical procedures
* Design, build, or maintain websites using authoring or scripting languages, content creation tools, management tools, and digital media
* Develop client relationships and communicate with clients to explain proposals, present research findings, establish specifications, or discuss status.

---

~~~
<div class="row">
<div class="col col-md-6 col-12">
~~~

## As a Geospatial (Data) Scientist

### Fieldwork & and mapping, analyzing, modeling, and predicting

* Publications
  * See [Researchgate](https://www.researchgate.net/profile/Shinichi_Kobara)
  * Near real-time oceanic glider mission viewers [16th chapter of the book: Ocean solutions, Earth solutions](https://esripress.esri.com/display/index.cfm?fuseaction=display&websiteID=295&moduleID=43)
  * Watershed analysis for Mesoamerican reef [Statistical analysis contribution (with GIS and R)](https://www.wri.org/research/watershed-analysis-mesoamerican-reef)

* 15+yrs GIS knowledge and skills

* Data visualization
  * 3D Visualization
  * Map Visualization [Gulf of Mexico](https://geo.gcoos.org)
  * Near real-time glider visualization
  * StoryMap (Share research with story-telling style with maps)
  * Websites
    * [RESTORE project](https://geo.gcoos.org/restore/) (Flask/MongoDB/Nuxtjs)
    * [Citizen Science](https://citizenscience.gcoos.org) (Plotly/Dash)
    * [Invasive Species observation map](https://tamu.maps.arcgis.com/apps/webappviewer/index.html?id=b52f2191c9d24f70aae8759bb8508af2) (ArcGIS WebApp Builder)
    * [Post-oil spill glider tracking map for NOAA ERMA Deepwater Gulf Response]

* Pattern Identification (Environmental Science)
  * Predict fish spawning aggregation sites with satellite data and shape analysis
  * Runoff/Sediment estimate on reefs (Hydrology)
  * Habitat suitability
  * Animal density and abundance spatial modeling
  * Hurricane damage assessment (image classification & analysis)

* Spatial Statistics
  * Statistical models for discrete, continuous, and regional data: Experience with Point pattern analysis for modeling discrete points, Geostatistics for modeling spatially continuous data, and Lattice analysis for modeling data.
  * Use exploratory data analysis techniques to identify meaningful relationships, patterns, or trends from complex data sets
  * Point pattern analysis

* Research(Toward Predicting and Protecting Fish Spawning Aggregation Sites)
  * 2weeks ~ 2months field trips
  * Seafloor mapping in Belize, Cayman Islands, Venezuela, Mexico
  * Predicting model based on bathymetry and satellite data
  * Satellite image processing for prediction
  * ArcGIS Model Builder, Python
  * Shell: automation (data download, upload, processing)

* Machine Learning
  * Fish Spawning aggregation site prediction
  * Satellite Image Processing (classification and object detection)
  * Completed coursework: ["Machine Learning"](https://www.coursera.org/account/accomplishments/certificate/VKTAWAA6JJDL), ["Mathematics for Machine Learning: Linear Algebra"](https://www.coursera.org/account/accomplishments/certificate/XPGFEAH4DF5D), ["Deep Learning Specialization"](https://www.coursera.org/account/accomplishments/specialization/certificate/KX3HD4MB5U9V), ["DeepLearning.AI TensorFlow Developer Specialization"](https://www.coursera.org/account/accomplishments/specialization/certificate/2YJUSEK8UXGE), ["Applied AI with DeepLearning](https://www.coursera.org/account/accomplishments/certificate/GZXLBHR276QG)
  * Machine Learning (**Traditional/Tabular data): Linear/Logistic Regression, Decision Tree Models, Ensemble methods, PCA

* Environment
  * Amazon SageMaker
  * Google Cloud VertexAI
  * Visual Studio Code
  * PyCharm
  * Jupyter

~~~
</div>
<div class="col col-md-6 col-12">
~~~

## As a Data Engineer

### DevOps, Web map application framework, Database, and Server

* Databases 10+yrs
  * PostgreSQL/PostGIS
  * DuckDB
  * MongoDB
  * MySQL
  * Google BigQuery

* DevOps 8+yrs
  * Docker (AWS ECR)
  * GitHub/GitHub Action (automation)
  * Shell (env/cron)
  * Hashicorp Vault (authentication)

* Cloud/Orchestration 5+yrs
  * Airflow
  * GCP (BigQuery/Cloud Storage)
  * AWS (Lambda/ECR/Fargate)

* Web: Developed and managed GCOOS Products & Services (Search & Find) [https://products.gcoos.org](https://products.gcoos.org)

* Web: Frontend development including faceted search, map applications
  * [Nuxtjs](https://nuxtjs.org)
  * [Plotly/Dash](https://dash.plotly.com/introduction)
  * ArcGIS Javascript API, Esri-leaflet
  * axios

* Develop multiple web applications (see [projects](https://opentheway.net/projects))
  * Generate web maps (e.g. [web maps](https://products.gcoos.org/webmaps))
  * Develop web map applications (e.g. [catalog](https://products.gcoos.org/catalogs))
  * Data catalog sites: 
    * [Geoportal](https://geo.gcoos.org/tc/geoportal/) (with Tomcat docker)
    * [ArcGIS-Hub](https://gisdata.gcoos.org)

* Web: Backend development 10+yrs
  * Docker (all apps are dockerized)
  * Django
  * FastAPI
  * MongoDB/PostgreSQL
  * Traefik (proxy and load balancer)
  * ArcGIS Server Enterprise (CentOS, Windows 2019, Amazon AWS) (e.g. [https://gis.gcoos.org](https://gis.gcoos.org))
  * Linux (Ubuntu): cron, Nginx

* Administrator role for ArcGIS Online for Texas A&M University
  * [https://tamu.maps.arcgis.com](https://tamu.maps.arcgis.com)
  * ArcGIS Hub (aka OpenData) Manager[https://gisdata.gcoos.org](https://gisdata.gcoos.org)

* Administrator role for ArcGIS Portal for GCOOS and ArcGIS Server Enterprise
  * System administration for both dedicated servers (both Linux and Windows ) and Cloud (AWS)
  * ArcGIS Server Enterprise Setup and Maintenance, including Proxy, SSL, WebAdaptor, PostgreSQL/PostGIS, Automatic map server update processing, cron/task scheduler
  * ArcGIS ImageServer (https://gis.gcoos.org/arcgis/rest/services/Images)
  * ArcGIS GeoEvent Server
  * ArcGIS Notebook Server (internal)

* Real-time GIS
  * Waveglider monitoring with ArcGIS GeoEvent Server (e.g. [Waveglider-Ocean-acidification-monitoring-test](https://geo.gcoos.org/data/maps/waveglider/usm/))

~~~
</div>
</div>
~~~

---

## Certificates and Licenses

* Geographic Information Systems (GIS) Professional (`GISP`) issued by GISCI
* Online course certificates such as Machine Learning, Deep Learning Specialization, and TensorFlow in Deployment are listed at [Linkedin](https://www.linkedin.com/in/shin-kobara/)

## Visa Status

** He is a `US Permanent resident` (a Green Card holder)

## Programing Language

Many repositories are private, but some are publicly available via [Github](https://github.com/otwn)

* Python 10+yrs
  * Analysis: pandas/scikit-learn
  * GIS: geopandas/shapely
  * Backend: Django/FastAPI
  * Visualization: Plotly/Dash
  * Cloud/Data Warehouse: Google Cloud API
  * Web Scraping
* SQL 8+yrs
  * PostGIS 8+yrs
  * DuckDB spatial 1yr
  * Google BigQuery 3yrs
* Julia 3+yrs
  * Analysis: DataFraems, MLJ, BetaML, Flux
  * Data: DuckDB
  * Visualization: Plots/Makie
  * GIS: GeoStats, ArchGDAL
  * TimeSeries: TSFramkes
* HTML/CSS/Javascript 8+yrs
  * Leaflet
  * HighCharts
  * DataTables
  * Bootstrap
