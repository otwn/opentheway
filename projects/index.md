@def title = "Projects"

~~~
<div class="row">
<div class="col col-sm-4 col-12">
    <div class="card shadow-sm">
        <img src="/assets/images/projects/fsa1to1.jpg" height="280" class="card-img-top" alt="fsa">
        <div class="card-body">
            <p class="card-text">
            <h3>Fish Spawning Aggregations</h3>
            </p>
            <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
                <a href="https://geo.gcoos.org/restore/" target="_blank"><button type="button" class="btn btn-sm btn-primary">View</button></a>
            </div>
            <!-- <small class="text-body-secondary">9 mins</small> -->
            </div>
        </div>
    </div>
</div>
<div class="col col-md-4 col-12">
    <div class="card shadow-sm">
    <img src="/assets/images/projects/webmapscatalog.jpg" class="card-img-top" alt="...">
    <div class="card-body">
        <p class="card-text">
        <h3>Web Map Example</h3>
        </p>
        <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
            <a href="/projects/webmaps"><button type="button" class="btn btn-sm btn-warning">View</button></a>
            <!-- <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button> -->
        </div>
        <!-- <small class="text-body-secondary">9 mins</small> -->
        </div>
    </div>
    </div>
</div>
</div> <!-- end of row -->
~~~
